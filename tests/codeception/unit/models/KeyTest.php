<?php

namespace tests\codeception\unit\models;

use Yii;
use Codeception\Specify;
use stdClass;

use app\models\Key;
use app\models\User;
use app\models\UserKeyRel;
use yii\codeception\TestCase;
use PHPUnit_Framework_TestCase;

class KeyTest extends TestCase
{

    protected $tester;

    private $testData;

    use Specify;

    protected function setUp(){
        parent::setUp();
        User::deleteAll();
        Key::deleteAll();
        UserKeyRel::deleteAll();
        $this->testData = new stdClass();
        $this->testData->email = "denniselite@live.com";
        $this->testData->username = null;
        $this->testData->is_approved = 0;
    }

    protected function tearDown()
    {
        User::deleteAll();
        Key::deleteAll();
        UserKeyRel::deleteAll();
        parent::tearDown();
    }

    public function testKeyGeneration()
    {
        $model = new Key();
        PHPUnit_Framework_TestCase::assertNotEmpty($model->genKey("string"));
    }

    public function testAddKey(){
        $model = new Key();
        $model->addKey($this->testData->email);

        $result = Key::find()->where(['key' => $model->authKey])->one();
        $this->specify('system should add new key', function () use ($result, $model) {
            PHPUnit_Framework_TestCase::assertTrue($result->key === $model->authKey);
        });
        $this->testData->keyId = $model->keyId;
    }

    public function testAddUserKeyRelation(){
        $this->testAddKey();
        $user = User::addUser($this->testData->email);
        $this->testData->id = $user->id;

        $model = new Key();
        $model->keyId = $this->testData->keyId;
        $model->addUserKeyRel($this->testData->id);
        $this->specify('system should add new user key relation', function () use ($model) {
            PHPUnit_Framework_TestCase::assertTrue(
                $this->testData->id === UserKeyRel::find()->where(['uid' => $this->testData->id])->one()->uid);
        });
    }

    public function testSuccessUpdateKey(){
        $this->testAddUserKeyRelation();
        $model = new Key();
        $model->keyId = $this->testData->keyId;
        $this->specify('system should update user key', function () use ($model) {
            PHPUnit_Framework_TestCase::assertTrue($model->updateKey($this->testData->email, $this->testData->id));
        });
        $this->testData->key = $model->authKey;
    }

    public function testUpdateWrongKey(){
        $this->testAddUserKeyRelation();
        $model = new Key();
        $model->keyId = $this->testData->keyId;
        $this->specify('system don\'t should update user key', function () use ($model) {
            PHPUnit_Framework_TestCase::assertFalse($model->updateKey($this->testData->email, 'wrong'));
        });
    }

    public function testDeleteKey()
    {
        $this->testSuccessUpdateKey();
        Key::deleteKey($this->testData->key);
        $this->specify('deleted key column should be mark as "1"', function () {
            PHPUnit_Framework_TestCase::assertTrue(
                1 === Key::find()->where(['key' => $this->testData->key])->one()->deleted);
        });
    }
}