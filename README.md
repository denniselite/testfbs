#Welcome to test yii2-application for FBS group company.

##Functions

This application used for user email authorization and configuration your profile name. On the Login page we can enter your email-address and we send secret link to you. We can login here using secret link on your email and setup or change your profile name.

##Structure

Pages: Home, Login. 

Controllers: SiteController (`/controllers/SiteController`);

###Models: 

All models are in `/models` folder.

* `Email` - generate and send email to user;

* `Index` - work with home page, settings for user profile;

* `Key` - work with authorization keys;

* `LoginForm` - methods for login page;

* `User` - work with identity objects;

* `UserKeyRel` - model for user-key relations;

###Views:

All views are in `/views/site` folder.

* `badKey` - when used bad link to login (with wrong/old key);

* `emailSent` - when email has been sent to user;

* `index` - home page;

* `login` - login page & login form;

* `profile` - user profile page (when user authorized successfully). Set name/change name.

##Database

* Table `keys` contains generated email keys;

* Table `users` contains added users;

* Table `user_key_rel` contains relations between users and keys;

##Tests

* Acceptance tests are in `/tests/codeception/acceptance`;

* Acceptance tests are in `/tests/codeception/functional`;

* Unit models tests are in `/tests/codeception/unit/models`;

###Acceptance tests coverage:

* Home page scenarios : login button, 'Go!' button;

* Login page scenarios : login form with different credentials;
 
###Acceptance tests coverage:

* Home page scenarios : login button, 'Go!' button;

* Login page scenarios : login form with different credentials;

###Unit tests coverage %: 

* Key model: 5/5 methods;

* LoginForm model: 1/2 methods;

* User model: 4/9 methods;

* Email model: 0/1 methods;

* UserKeyRel does not contain methods for testing;