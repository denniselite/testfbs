<?php
/**
 * Created by PhpStorm.
 * User: Denniselite
 * Date: 24.04.15
 * Time: 10:00
 */

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{

    public static function tableName(){
        return 'users';
    }


    public static function addUser($email){
        $user = new User();
        $user->email = $email;
        $user->save();
        return $user;
    }

    public static function getUser($userKey){
        return self::findIdentityByAccessToken($userKey);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return new static(self::findOne($id));
    }

    public static function findIdentifyByEmail($email){
        $user = self::find()->where(['email' => $email])->one();
        return (!empty($user)) ? $user : false;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($userKey, $type = null)
    {
        $key = Key::find()->where(['key' => $userKey])->one();
        $uidKeyPair  = UserKeyRel::find()->where(['key' => $key->id])->one();
        return new static(self::findOne($uidKeyPair->uid));
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        $uidKeyPair = UserKeyRel::find()->where(['uid' => $this->getId()])->one();
        if (empty($uidKeyPair)) {
            return false;
        }
        $authKey = Key::find($uidKeyPair->key)->one();
        return (!empty($authKey)) ? $authKey->key : false;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        $keyObj = Key::find()->where(['key' => $authKey])->one();
        return (!empty($keyObj) && ($keyObj->deleted === 0)) ? true : false;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return null;
    }
}
