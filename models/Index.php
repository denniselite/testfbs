<?php
/**
 * Created by PhpStorm.
 * User: Denniselite
 * Date: 24.04.15
 * Time: 18:33
 */

namespace app\models;
use yii\base\Model;
use Yii;

class Index extends Model{

    public $name;

    public function rules(){
        return [
            ['name', 'required']
        ];
    }

    public function setName(){
        $user = User::findOne(Yii::$app->user->identity->id);
        $user->username = $this->name;
        $user->save();
        return true;
    }
}