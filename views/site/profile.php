<?php
/**
 * Created by PhpStorm.
 * User: Denniselite
 * Date: 24.04.15
 * Time: 18:46
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'FBS Group test application';
?>
<div class="site-index">

    <div class="jumbotron">
        <?php if (isset(Yii::$app->user->identity->username) && (Yii::$app->user->identity->username !== '')) { ?>
            <h1>Hi, <?= Yii::$app->user->identity->username;?>!</h1>
        <?php } else { ?>
            <h1>Hi, there!</h1>
        <?php } ?>

        <?php if (Yii::$app->user->identity->username == '') { ?>
        <p class="lead">What is your name?</p>
        <?php } else { ?>
        <p class="lead">Change your name</p>
        <?php } ?>
        <?php if (!Yii::$app->user->isGuest) { ?>
            <?php $form = ActiveForm::begin();?>

            <?=$form->field($model, 'name')->label('Your name'); ?>

            <div class="form-group">
                <?=Html::submitButton('Set name', ['class' => 'btn btn-primary']); ?>
            </div>

            <?php ActiveForm::end(); ?>

        <?php } ?>
    </div>
</div>
