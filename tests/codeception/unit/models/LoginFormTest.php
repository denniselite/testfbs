<?php

namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use Codeception\Specify;

use app\models\LoginForm;

class LoginFormTest extends TestCase
{
    use Specify;

    protected function setUp(){
        parent::setUp();
    }

    protected function tearDown()
    {
        Yii::$app->user->logout();
        parent::tearDown();
    }

    public function testLoginNoEmail()
    {
        $model = new LoginForm(['email' => '']);

        $this->specify('user should not be able to login, when email is empty', function () use ($model) {
            expect('model should not send email', $model->loginRequest())->false();
            expect('user should not be logged in', Yii::$app->user->isGuest)->true();
        });
    }

    public function testLoginWrongEmail()
    {
        $model = new LoginForm(['email' => 'wrongemail']);

        $this->specify('user should not be able to login, when email is wrong', function () use ($model) {
            expect('model should not send email', $model->loginRequest())->false();
            expect('user should not be logged in', Yii::$app->user->isGuest)->true();
        });
    }

    public function testLoginCorrectEmail()
    {
        $this->markTestSkipped('must be revisited.');
        $model = new LoginForm(['email' => 'denniselite@live.com']);
        $this->specify('system should be able to send email link from login with correct email format', function () use ($model) {
            expect('model should send email to user', $model->loginRequest())->true();
            expect('user should not be logged in', Yii::$app->user->isGuest)->true();
        });
    }

}
