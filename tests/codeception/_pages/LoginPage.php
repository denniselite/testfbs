<?php

namespace tests\codeception\_pages;

use yii\codeception\BasePage;

/**
 * Represents login page
 * @property \AcceptanceTester|\FunctionalTester $actor
 */
class LoginPage extends BasePage
{
    public $route = 'site/login';

    /**
     * @param string $email
     */
    public function login($email)
    {
        $this->actor->fillField('input[name="LoginForm[email]"]', $email);
        $this->actor->click('login-button');
    }
}
