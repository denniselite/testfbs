<?php

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that home page works');
$I->amOnPage(Yii::$app->homeUrl);
$I->see('FBS Group test app');
$I->amGoingTo('Go to Login page through top menu');
$I->seeLink('Login');
$I->click('Login');
$I->see('Login', 'h1');
$I->amGoingTo('Back to home');
$I->seeLink('Home');
$I->click('Home');
$I->see('Hi, there!', 'h1');
$I->seeLink('Go!');
$I->click('Go!');
$I->see('Login', 'h1');