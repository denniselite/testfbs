<?php
/**
 * Created by PhpStorm.
 * User: Denniselite
 * Date: 24.04.15
 * Time: 20:58
 */

namespace app\models;
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;

class Email{

    public static function sendEmail($authKey, $email){
        $uri = urldecode(Url::toRoute(['site/login', 'key' => $authKey], true));
        $body = Html::a('Go!', $uri);
        Yii::$app->mailer->compose()
            ->setFrom($email)
            ->setTo($email)
            ->setSubject('SingIn Link')
            ->setHtmlBody($body)
            ->send();
    }
}