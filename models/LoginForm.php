<?php

namespace app\models;
use yii\base\Model;
use Yii;
/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $email;
    public $newUser = false;

    private $user;
    private $rememberMe = true;

    public function rules(){
        return [
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    public function loginRequest(){
        if ($this->validate()){
            $key = new Key();
            $this->user = User::findIdentifyByEmail($this->email);
            if (empty($this->user)){
                $this->user = User::addUser($this->email);
                $this->newUser = true;
                $key->addKey($this->email);
                $key->addUserKeyRel($this->user->id);
            } else {
                if (!$key->updateKey($this->email, $this->user->id)){
                    $key->addKey($this->email);
                    $key->addUserKeyRel($this->user->id);
                }
            }
            Email::sendEmail($key->authKey, $this->email);
            return true;
        } else{
            return false;
        }
    }

    public function signIn($authKey){
        $user = new User();
        if ($user->validateAuthKey($authKey)){
            Yii::$app->user->login(User::getUser($authKey), $this->rememberMe ? 3600*24*30 : 0);
            Key::deleteKey($authKey);
            return true;
        } else{
            return false;
        }
    }
}
