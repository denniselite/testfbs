<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please enter your email address</p>

    <?php $form = ActiveForm::begin();?>

    <?=$form->field($model, 'email')->label('Your email'); ?>

    <div class="form-group">
        <?=Html::submitButton('Submit', ['class' => 'btn btn-primary', 'id' => 'login-button']); ?>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="col-lg-offset-1" style="color:#999;">
        We will send email from you with login instructions
    </div>
</div>
