<?php

use tests\codeception\_pages\LoginPage;

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that login works');
$I->amOnPage('index.php?r=site/login');
$I->see('Login');
$I->amGoingTo('try to login with empty credentials');
$I->fillField('input[name="LoginForm[email]"]', '');
$I->expectTo('see validations errors');
$I->see('Email cannot be blank.');

$I->amGoingTo('try to login with wrong credentials');
$I->fillField('input[name="LoginForm[email]"]', 'NoEmail');
$I->expectTo('see validations errors');
$I->see('Email is not a valid email address.');

$I->amGoingTo('try to login with correct credentials');
$I->fillField('input[name="LoginForm[email]"]', 'denniselite@live.com');
$I->click('Submit');

$I->expectTo('see Success Page');
$I->see('Success!');

