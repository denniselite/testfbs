<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'FBS Group test application';
?>
<div class="site-index">

    <div class="jumbotron">
        <?php if (isset(Yii::$app->user->identity->username) && (Yii::$app->user->identity->username !== '')) { ?>
            <h1>Hi, <?= Yii::$app->user->identity->username;?>!</h1>
        <?php } else { ?>
        <h1>Hi, there!</h1>
        <?php } ?>
        <p class="lead">For next steps, click the login button</p>

        <?= Html::a('Go!', ['site/login'], ['class' => 'btn btn-lg btn-success']) ?>
    </div>
</div>
