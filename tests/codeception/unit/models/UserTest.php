<?php

namespace tests\codeception\unit\models;

use Yii;
use Codeception\Specify;

use app\models\User;
use yii\codeception\TestCase;
use PHPUnit_Framework_TestCase;
use stdClass;

class UserTest extends TestCase
{
    use Specify;

    private $testData;

    protected function setUp()
    {
//        $this->markTestSkipped('must be revisited.');
        parent::setUp();
        User::deleteAll();
    }

    protected function tearDown()
    {
        User::deleteAll();
        parent::tearDown();
    }

    public function testAddUser(){

        $this->testData = new stdClass();
        $this->testData->email = "denniselite@live.com";
        $this->testData->username = null;
        $this->testData->is_approved = 0;
        $user = User::addUser($this->testData->email);
        $result =
            ($user->email === $this->testData->email) &&
            ($user->username === $this->testData->username) &&
            ($user->is_approved == $this->testData->is_approved);
        $this->specify('system should add new key', function () use ($result) {
            PHPUnit_Framework_TestCase::assertTrue($result);
        });
        $this->testData->id = $user->id;
    }


    public function testFindUserByTrueEmail(){
        $this->testAddUser();
        $user = User::findIdentifyByEmail($this->testData->email);
        $result =
            ($user->email === $this->testData->email) &&
            ($user->username === $this->testData->username) &&
            ($user->is_approved == $this->testData->is_approved);
        $this->specify('user should be find by Email', function () use ($result) {
            PHPUnit_Framework_TestCase::assertTrue($result);
        });
    }

    public function testFindUserByWrongEmail(){
        $result = User::findIdentifyByEmail("some@email.com");
        $this->specify('user don\'t should be find by Email', function () use ($result) {
            PHPUnit_Framework_TestCase::assertFalse($result);
        });
    }

    public function testFindUserById(){
        $this->testAddUser();
        $user = User::findIdentity($this->testData->id);
        $result =
            ($user->email === $this->testData->email) &&
            ($user->username === $this->testData->username) &&
            ($user->is_approved === $this->testData->is_approved) &&
            ($user->id === $this->testData->id);
        $this->specify('user should be find by ID', function () use ($result) {
            PHPUnit_Framework_TestCase::assertTrue($result);
        });
    }
}
