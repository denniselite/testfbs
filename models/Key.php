<?php
/**
 * Created by PhpStorm.
 * User: Denniselite
 * Date: 24.04.15
 * Time: 8:42
 */

namespace app\models;

use yii\db\ActiveRecord;

class Key extends ActiveRecord
{

    public $authKey;
    public $keyId;
    public static function tableName(){
        return 'keys';
    }

    public function genKey($email){
        $this->key = hash('sha256', $email . microtime());
        return $this->key;
    }

    public static function deleteKey($key){
        $key = self::find()->where(['key' => $key])->one();
        $key->deleted = 1;
        $key->save();
        $userKeyPair = UserKeyRel::find()->where(['key' => $key->id])->one();
        $userKeyPair->delete();
    }

    public function addKey($email){
        $key = new Key();
        $this->authKey = $key->genKey($email);
        $key->save();
        $this->keyId = $key->id;
    }

    public function updateKey($email, $uid){
        $userKeyPair = UserKeyRel::find()->where(['uid' => $uid])->one();
        if (empty($userKeyPair)) {
            return false;
        }
        $key = Key::findOne($userKeyPair->key);
        $key->key = $key->genKey($email);
        $this->authKey = $key->key;
        $key->save();
        $this->keyId = $key->id;
        $userKeyPair->key = $this->keyId;
        $userKeyPair->save();
        return true;
    }

    public function addUserKeyRel($uid){
        $rel = new UserKeyRel();
        $rel->uid = $uid;
        $rel->key = $this->keyId;
        $rel->save();
    }
}
