<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_021922_init_schema extends Migration
{
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => Schema::TYPE_PK,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'username' => Schema::TYPE_STRING,
            'is_approved' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'UNIQUE INDEX (`email`)'
        ]);

        $this->createTable('keys', [
            'id' => Schema::TYPE_PK,
            'key' => Schema::TYPE_STRING . ' NOT NULL',
            'deleted' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'UNIQUE INDEX (`key`)'
        ]);

        $this->createTable('user_key_rel', [
            'key' => Schema::TYPE_INTEGER . ' NOT NULL',
            'uid' => Schema::TYPE_INTEGER . ' NOT NULL',
            'PRIMARY KEY (`key`, uid)',
            'UNIQUE INDEX (`key`)',
            'UNIQUE INDEX (`uid`)'
        ]);

        $this->addForeignKey('user_key_rel_key_fk', 'user_key_rel', 'key', 'keys', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('user_key_rel_uid_fk', 'user_key_rel', 'uid', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('user_key_rel_key_fk', 'user_key_rel');
        $this->dropForeignKey('user_key_rel_uid_fk', 'user_key_rel');
        $this->dropTable('user_key_rel');
        $this->dropTable('users');
        $this->dropTable('keys');
    }
}
