<?php

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that home page works');
$I->amOnPage(Yii::$app->homeUrl);
$I->see('FBS Group test app');

//Top menu link testing
$I->amGoingTo('Login page using top menu');
$I->seeLink('Login');
$I->click('Login');
$I->see('Login', 'h1');

//Go back
$I->amGoingTo('Back to home');
$I->seeLink('Home');
$I->click('Home');
$I->see('Hi, there!', 'h1');

//"Go!" link testing
$I->amGoingTo('Login page using "Go!" button');
$I->seeLink('Go!');
$I->click('Go!');
$I->see('Login', 'h1');
