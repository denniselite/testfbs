<?php
/**
 * Created by PhpStorm.
 * User: Denniselite
 * Date: 24.04.15
 * Time: 19:27
 */
?>
<div class="site-index">

    <div class="jumbotron">
       <h2>Success!</h2>
        <?php if ($model->newUser) { ?>
        <p>
            We sent email from You with SignIn link. <br/>
            Now we can complete the registration after reading Your mail.
        </p>
        <?php } else {?>
        <p>
            Welcome back! We sent email from You with SignIn link.
        <?php } ?>
        </p>
    </div>
</div>